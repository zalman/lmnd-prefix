# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=2

inherit eutils flag-o-matic git

EGIT_REPO_URI="https://github.com/lkastner/polymake_algebra.git"
DESCRIPTION="extension for polymake to interface with singular"
HOMEPAGE="http://polymake.org"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"

DEPEND=">=sci-mathematics/polymake-9990
   >=sci-libs/libsingular-3.1.5"

RDEPEND="${DEPEND}"

S="${WORKDIR}"/polymake_algebra

src_configure () {
	# this adds the build infrastructure for the extension and can take
	# more arguments for configuration
	perl ${EROOT}/usr/share/polymake/support/configure_ext.pl \
		${EROOT}/usr/share/polymake \
		${EROOT}/usr/$(get_libdir)/polymake ${S} || die \
		"could not configure extension"
}

src_install () {
	# some overrides to make DESTDIR work for extension installation
    echo 'override InstallTop := ${DESTDIR}${InstallTop}
override InstallArch := ${DESTDIR}${InstallArch}
override InstallBin := ${DESTDIR}${InstallBin}
override InstallInc := ${DESTDIR}${InstallInc}
override InstallLib := ${DESTDIR}${InstallLib}
override InstallDoc := ${DESTDIR}${InstallDoc}' >> ${S}/build.*/conf.make
	emake DESTDIR="${D}" install || die "install failed"
}

pkg_postinst(){
	elog "This is a preliminary version of the libsingular interface."
	elog "It is not complete and may contain (lots of) bugs."
}
