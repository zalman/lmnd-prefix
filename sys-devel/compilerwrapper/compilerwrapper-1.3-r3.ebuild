# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit eutils

DESCRIPTION=""
HOMEPAGE="https://bitbucket.org/vbraun/compilerwrapper"
VERSION="be20111027"
SRC_URI="http://www.lmona.de/files/distfiles/compilerwrapper-$VERSION.tar.gz"
S="${WORKDIR}/compilerwrapper-$VERSION"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64-linux x86-linux ppc-macos x64-macos"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_configure() {
	econf --with-ccpath=/usr/bin --with-ldpath=/usr/bin
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
	dodoc AUTHORS NEWS README TODO
}
